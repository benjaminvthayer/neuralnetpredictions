OUTDATED-----DELETE THIS FILE

library(neuralnet)
library("Metrics")
## Find ARBRC_Precipitation ##
allData<-read.csv('Find_ARBRC_Precipitation_All.csv')
testTrain<-read.csv('Find_ARBRC_PrecipitationTestTrain.csv')
question<-read.csv('Find_ARBRC_PrecipitationMissingData.csv')
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

#must manually correct scenario each run
numPartitions<-20
thresh<-".02"
nodes<-"2"
algo<-"backprop"
outputVarNum<-3#column ARBRC_Precipitation is stored in
variables<-11

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max)
mins <- apply(allData, 2, min)
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("ARBRC_Precipitation_in_corrected ~", paste(n[!n %in% "ARBRC_Precipitation_in_corrected"], collapse = " + ")))


#Create container to store stats of best ANN
resultsPre <- rep(0, 7)
results <- array(resultsPre, c(2,7))

results[1,1]<-"Train RMSE"
results[1,2]<-"Train R^2"
results[1,3]<-"Test RMSE"
results[1,4]<-"Test R^2"
results[1,5]<-"Algorithm"
results[1,6]<-"Hidden Nodes"
results[1,7]<-"Threshold"

results[2,5]<-algo
results[2,6]<-nodes
results[2,7]<-thresh

##create 100 ANNs with same scenario, remember ANN with best Weights
bestaccuracy<-9999
for(x in 1:100){
  #must manually correct learningrate each run
  set.seed(x)
  trainScaledRandomized<- trainScaled[sample(nrow(trainScaled)),]
  
  nn <- neuralnet(f,data=trainScaledRandomized,hidden=nodes,linear.output=T,stepmax=1e8,learningrate=.001,threshold=thresh,algorithm=algo)
  
  trainScaledOrdered<-trainScaled[order(as.numeric(rownames(trainScaled))),,drop=FALSE]
  trainOrdered<-train[order(as.numeric(rownames(train))),,drop=FALSE]

  ##compute test stats
  pr.nn <- compute(nn,testScaled[,1:(variables-1)])
  testPredicted<- (pr.nn$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be
  testActual<-test[,outputVarNum]
  currentRMSTest<-rmse(testActual,testPredicted)
  testRsqr<-1 - (sum((testActual-testPredicted )^2)/sum((testActual-mean(testActual))^2))

  #compute train stats
  pr.nn <- compute(nn,trainScaledOrdered[,1:(variables-1)])
  trainPredicted<- (pr.nn$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be
  trainActual<-trainOrdered[,outputVarNum]
  RMSTrain<-rmse(trainActual,trainPredicted)
  trainRsqr<-1 - (sum((trainActual-trainPredicted )^2)/sum((trainActual-mean(trainActual))^2))##r^2

  if(trainRsqr>0 && testRsqr>0){
    currentAccuracyScore<-((1/testRsqr)+currentRMSTest + (1/trainRsqr)+RMSTrain )/4#lower is better
    } else{
    currentAccuracyScore<-9999
    }

  if(currentAccuracyScore<bestaccuracy){
    bestaccuracy<-currentAccuracyScore
    bestnn<-nn
    results[2,1]<-RMSTrain
    results[2,2]<-trainRsqr
    results[2,3]<-currentRMSTest
    results[2,4]<-testRsqr
    bestTrain<-trainPredicted
    bestTest<-testPredicted
  }
}


#Compute Missing Data
questionComputed <- compute(bestnn,questionScaled[,1:(variables-1)])
MissingData <- (questionComputed$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be


#Save Report of ANN
write.csv(results,file="ARBRC_PrecF16_Results.csv")
#Save weights
capture.output(bestnn$weights, file = "ARBRC_PrecF16_Weights.txt")
#save the ANN in a format to be re-loaded by r
saveRDS(file="ARBRC_PrecF16_r_readable.txt",bestnn)
#Save Prediction For Missing Data
write.csv(MissingData,file="ARBRC_PrecF16_FinalMissingPrediction.csv")

write.csv(train,file="ARBRC_TempF16_Train.csv")
write.csv(bestTrain,file="ARBRC_TempF16_TrainPrediction.csv")
write.csv(test,file="ARBRC_TempF16_Test.csv")
write.csv(bestTest,file="ARBRC_TempF16_TestPrediction.csv")
