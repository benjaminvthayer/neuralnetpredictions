
library(neuralnet)
library("Metrics")
## Find ARBRC PRECIPITATION ##
allData<-read.csv('Find_ARBRC_Precipitation_All.csv')
testTrain<-read.csv('Find_ARBRC_PrecipitationTestTrain.csv')
question<-read.csv('Find_ARBRC_PrecipitationMissingData.csv')
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)


##create containers for all test and train values
numPartitions<-100
variables<-11
statNum<-8##number of stats
trainSize<-468
testSize<-118
thresh<-".02"
nodes<-"2"
algo<-"backprop"
outputVarNum<-9#column ARBRC_Precipitation is stored in

#create container for test and train
testListsPre <- rep(0, testSize*variables*numPartitions)
testLists  <- array(testListsPre, c(testSize,variables,numPartitions))
trainListsPre <- rep(0, trainSize*variables*numPartitions)
trainLists <- array(trainListsPre, c(trainSize,variables,numPartitions))
colnames(testLists)<-colnames(allData)
colnames(trainLists)<-colnames(allData)


#trainlists is [468,11,20]  ([days,weatherRecordingTypes,partitions])
#testlists is [118,11,20]  ([days,weatherRecordingTypes,partitions])
#testTrain is [586,11]   ([days,weatherRecordingTypes])
## partition test and train
for(partitionNum in 1:numPartitions){
  smp_size <- floor(0.80 * nrow(testTrain))
  set.seed(partitionNum)
  train_ind <- sort(sample(seq_len(nrow(testTrain)), size = smp_size))
  for(dataColumn in 1:variables){
    traini<-1#day
    testi<-1#day
    for(day in 1:(trainSize+testSize)){
      if( day %in% train_ind){
        trainLists[traini,dataColumn,partitionNum]<-testTrain[day,dataColumn]
        traini<-traini+1
      }
      else{
        testLists[testi,dataColumn,partitionNum]<-testTrain[day,dataColumn]
        testi<-testi+1
      }
    }
  }
}

##create containers for scaled test and train
testScaledsPre <- rep(0, testSize*variables*numPartitions)
testScaleds <-  array(testScaledsPre, c(testSize,variables,numPartitions))
trainScaledsPre <-rep(0, trainSize*variables*numPartitions)
trainScaleds <- array(trainScaledsPre, c(trainSize,variables,numPartitions))
colnames(testScaleds)<-colnames(allData)
colnames(trainScaleds)<-colnames(allData)

##Normalize The Data
maxs <- apply(allData, 2, max)
mins <- apply(allData, 2, min)

for(p in 1:numPartitions){
  trainScaleds[,,p]<-scale(trainLists[,,p], center = mins, scale = maxs - mins)
}
for(p in 1:numPartitions){
  testScaleds[,,p]<-scale(testLists[,,p], center = mins, scale = maxs - mins)
}

##Create the formula
n <- names(testTrain)
f <- as.formula(paste("ARBRC_Precipitation_in_corrected ~", paste(n[!n %in% "ARBRC_Precipitation_in_corrected"], collapse = " + ")))

##container for results
##statistic numbers: 1 partitionSeed,2 algorithm,3 hiddenNodes,4 threshol,5 rmse training, 6 r^2 train,7 rmse test, 8 r^2 test
scenarioStatsPre <- rep(0, numPartitions)
scenarioStats <- array(scenarioStatsPre, c(statNum,numPartitions))

bestAccuracyScore<-999999#average of test rootmeansquared, train rms, 1/test r^2, and 1/train r^2

for(partitionSeedPos in 1:numPartitions){

  nn <- neuralnet(f,data=trainScaleds[,,partitionSeedPos],hidden=c(nodes),linear.output=T,stepmax=1e8,threshold=thresh,learningrate=.001,algorithm=algo)
  
  
  ##compute test stats
  pr.nn <- compute(bestnn,testScaleds[,1:10,partitionSeedPos])
  testPredicted<- (pr.nn$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be
  testActual<-testLists[,outputVarNum,partitionSeedPos]
  RMSTest<-rmse(testActual,testPredicted)
  testRsqr<-1 - (sum((testActual-testPredicted )^2)/sum((testActual-mean(testActual))^2))
  
  #compute train stats
  pr.nn <- compute(bestnn,trainScaleds[,1:10,partitionSeedPos])
  trainPredicted<- (pr.nn$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be
  trainActual<-trainLists[,outputVarNum,partitionSeedPos]
  RMSTrain<-rmse(trainActual,trainPredicted)
  trainRsqr<-1 - (sum((trainActual-trainPredicted )^2)/sum((trainActual-mean(trainActual))^2))##r^2
  
  if(testRsqr>0 &&trainRsqr>0){
    currentAccuracyScore<-((.05/testRsqr)+RMSTest + (.05/trainRsqr)+RMSTrain )/4
  }else{
    currentAccuracyScore<-99999
  }
  
  
  ##store if best
  if(currentAccuracyScore<bestAccuracyScore){
    bestnn<-nn
    bestRMSTest<-currentRMSTest
    bestAccuracyScore<-currentAccuracyScore
    bestRMSTest<-RMSTest
    bestRMSTrain<-RMSTrain
    bestRsqrTest<-testRsqr
    bestRsqrTrain<-trainRsqr
    bestTrain<-trainScaleds[,1:10,partitionSeedPos]
    bestTest<-testScaleds[,1:10,partitionSeedPos]
    bestTestPredicted<-testPredicted
    bestTrainPredicted<-trainPredicted
  }
  ##store
  ##statistic numbers: 1 partitionSeed,2 algorithm,3 hiddenNodes,4 threshold,5 rmse training, 6 r^2 train,7 rmse test, 8 r^2 test
  scenarioStats[1,partitionSeedPos]<- partitionSeedPos
  scenarioStats[2,partitionSeedPos]<- algo
  scenarioStats[3,partitionSeedPos]<- nodes
  scenarioStats[4,partitionSeedPos]<- thresh
  scenarioStats[5,partitionSeedPos]<- RMSTrain
  scenarioStats[6,partitionSeedPos]<- trainRsqr
  scenarioStats[7,partitionSeedPos]<- RMSTest
  scenarioStats[8,partitionSeedPos]<- (-1*testRsqr)
  rownames(scenarioStats)<-c("Partition","Algorithm","Nodes","Thresh","RMS Train","Train R^2","RMS Test","Test R^2")
}


#Compute Missing Data
questionComputed <- compute(bestnn,questionScaled[,1:(variables-1)])
MissingData <- (questionComputed$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be



pr.nn <- compute(bestnn,testScaleds[,1:10,2])
testPredicted<- (pr.nn$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be


pr.nn <- compute(bestnn,trainScaleds[,1:10,2])
trainPredicted<- (pr.nn$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be






#Save Report of ANN
write.csv(scenarioStats,file="ARBRC_PrecF16_Results.csv")
#Save weights
capture.output(bestnn$weights, file = "ARBRC_PrecF16_Weights.txt")
#save the ANN in a format to be re-loaded by r
saveRDS(file="ARBRC_PrecF16_r_readable.txt",bestnn)
#Save Prediction For Missing Data
write.csv(MissingData,file="ARBRC_PrecF16_FinalMissingPrediction.csv")

write.csv(bestTrain,file="ARBRC_PrecipF16_Train.csv")
write.csv(bestTrainPredicted,file="ARBRC_PrecipF16_TrainPrediction.csv")
write.csv(bestTest,file="ARBRC_PrecipF16_Test.csv")
write.csv(bestTestPredicted,file="ARBRC_PrecipF16_TestPrediction.csv")

#write.table(s, "clipboard-16384", sep="\t", row.names=FALSE)
#write.table(testTrain, "clipboard-16384", sep="\t", row.names=FALSE)
#write.table(testLists, "clipboard-16384", sep="\t", row.names=FALSE)
#write.table(trainLists, "clipboard-16384", sep="\t", row.names=FALSE)
#write.table(trainScaleds, "clipboard-16384", sep="\t", row.names=FALSE)
#write.table(testScaleds, "clipboard-16384", sep="\t", row.names=FALSE)
