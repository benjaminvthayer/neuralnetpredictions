##This file is for re-opening an r-readable previously created
library(neuralnet)
library("Metrics")

##setwd("C:\\Users\\Ben\\Documents\\gitRepos\\ssol\\neuralnetpredictions\\Sal_Weather_Station_Nets\\Find_KFC_Temperature\\Formulation16")
nn<-readRDS(file="ARBRC_PrecF16_r_readable.txt")

## Find KFC Temperature ##
allData<-read.csv('Find_ARBRC_Precipitation_All.csv')
testTrain<-read.csv('Find_ARBRC_PrecipitationTestTrain.csv')
question<-read.csv('Find_ARBRC_PrecipitationMissingData.csv')
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max)
mins <- apply(allData, 2, min)
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

#compute missing
questionComputed <- compute(nn,questionScaled[,1:10])
MissingData <- (questionComputed$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be
write.csv(MissingData,file="FromExisting_ARBRC_PrecipF16_MissingPrediction.csv")

#compute train
trainComputed <- compute(nn,trainScaled[,1:10])
TrainData <- (trainComputed$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be
#write.csv(TrainData,file="FromExisting_ARBRC_TempF16_TrainPrediction.csv")
#write.csv(train,file="FromExisting_ARBRC_TempF16_Train.csv")

#compute test
testComputed <- compute(nn,testScaled[,1:10])
TestData <- (testComputed$net.result)*(max(allData$ARBRC_Precipitation_in_corrected)-min(allData$ARBRC_Precipitation_in_corrected))+min(allData$ARBRC_Precipitation_in_corrected)##What the net predicts test should be
#write.csv(TestData,file="FromExisting_ARBRC_TempF16_TestPrediction.csv")
#write.csv(test,file="FromExisting_ARBRC_TempF16_Test.csv")
