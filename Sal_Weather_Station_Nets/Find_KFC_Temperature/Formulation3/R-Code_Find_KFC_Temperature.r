library(neuralnet)
## Find ARBRC Radiation ##
allData <-Find_KFC_TemperatureTestTrain
allData <- Filter(function(x)!all(is.na(x)), allData)
question <- Find_KFC_Temperature_MissingData
question <- Filter(function(x)!all(is.na(x)), question)

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(allData))
set.seed(123)
train_ind <- sample(seq_len(nrow(allData)), size = smp_size)
train <- allData[train_ind, ]
test <- allData[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("Temperature ~", paste(n[!n %in% "Temperature"], collapse = " + ")))
nn <- neuralnet(f,data=trainScaled,hidden=c(3,2),linear.output=T)

##Transform back to Usable Values
pr.nn <- compute(nn,testScaled[,1:6])
testResults <- (pr.nn$net.result)*(max(allData$Temperature)-min(allData$Temperature))+min(allData$Temperature)##What the net predicts test should be

##Load Missing Data
questionComputed <- compute(nn,questionScaled[,1:6])
results <- (questionComputed$net.result)*(max(allData$Temperature)-min(allData$Temperature))+min(allData$Temperature)##What the net predicts test should be
View(results)
plot(nn)
write.table(test, "clipboard", sep="\t", row.names=FALSE)
write.table(testResults, "clipboard", sep="\t", row.names=FALSE)
write.table(results, "clipboard", sep="\t", row.names=FALSE)
