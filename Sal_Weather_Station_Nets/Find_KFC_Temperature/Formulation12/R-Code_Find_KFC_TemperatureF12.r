library(neuralnet)
## Find KFC Radiation ##
allData<-Find_KFC_TemperatureAll
testTrain <-Find_KFC_TemperatureTestTrain
question <- Find_KFC_TemperatureMissingData
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("KFC_Temperature_F ~", paste(n[!n %in% "KFC_Temperature_F"], collapse = " + ")))
nn <- neuralnet(f,data=trainScaled,hidden=c(6),linear.output=T,stepmax=1e9,threshold=.03)


##Transform back to Usable Values
pr.nn <- compute(nn,testScaled[,1:11])
testResults<- (pr.nn$net.result)*(max(allData$KFC_Temperature_F)-min(allData$KFC_Temperature_F))+min(allData$KFC_Temperature_F)##What the net predicts test should be

##Load Missing Data
questionComputed <- compute(nn,questionScaled[1:11])
results <- (questionComputed$net.result)*(max(allData$KFC_Temperature_F)-min(allData$KFC_Temperature_F))+min(allData$KFC_Temperature_F)##What the net predicts test should be

nn$weights
setwd("C:\\Users\\Ben\\Documents\\gitRepos\\ssol\\neuralnetpredictions\\Sal_Weather_Station_Nets\\Find_KFC_Temperature\\Formulation12")
saveRDS(file="r_readable_net.txt",nn)
nn<-load(file="r_readable_net.rda",nn)
plot(nn,arrow.length = 0.2,show.weights = FALSE)
write.table(test, "clipboard", sep="\t", row.names=FALSE)
write.table(testResults, "clipboard", sep="\t", row.names=FALSE)
write.table(results, "clipboard", sep="\t", row.names=FALSE)
