library(neuralnet)
## Find ARBRC Radiation ##
allData<-Find_ARBRC_Radiation_All
testTrain <-Find_ARBRC_RadiationTestTrain
question <- Find_ARBRC_RadiationMissing
allData <- Filter(function(x)!all(is.na(x)), allData)



##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("ARBRC_Radiation ~", paste(n[!n %in% "ARBRC_Radiation"], collapse = " + ")))
nn <- neuralnet(f,data=trainScaled,hidden=c(17,8),linear.output=T,stepmax=1e8)

##Transform back to Usable Values
pr.nn <- compute(nn,testScaled[,1:29])
testResults3<- (pr.nn$net.result)*(max(allData$ARBRC_Radiation)-min(allData$ARBRC_Radiation))+min(allData$ARBRC_Radiation)##What the net predicts test should be

##Load Missing Data
questionComputed <- compute(nn,questionScaled[,1:29])
results <- (questionComputed$net.result)*(max(allData$ARBRC_Radiation)-min(allData$ARBRC_Radiation))+min(allData$ARBRC_Radiation)##What the net predicts test should be


View(results)
plot(nn)
write.table(test, "clipboard", sep="\t", row.names=FALSE)
write.table(testResults3, "clipboard", sep="\t", row.names=FALSE)
write.table(results, "clipboard", sep="\t", row.names=FALSE)
