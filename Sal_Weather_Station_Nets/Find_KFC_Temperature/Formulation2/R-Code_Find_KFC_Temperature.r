## Find KFC Temperature ##
allData <-Find_KFC_Temperature
question <- Find_KFC_Temperature_MissingData
trainStart <-1
trainEnd<-600
testStart<-601
testEnd<-682
searchName<-"KFC_Temperature"

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min)
scaled <- as.data.frame(scale(allData, center = mins, scale = maxs - mins))
train_ <- scaled[trainStart:trainEnd,]
test_ <- scaled[testStart:testEnd,]

##Create the Net
n <- names(train_)
f <- as.formula(paste("KFC_Temperature ~", paste(n[!n %in% "KFC_Temperature"], collapse = " + ")))
nn <- neuralnet(f,data=train_,hidden=c(8),linear.output=T)

##Transform back to Usable Values
pr.nn <- compute(nn,test_[,1:10])
pr.nn_ <- pr.nn$net.result*(max(train_$KFC_Temperature)-min(train_$KFC_Temperature))+min(train_$KFC_Temperature)##values in neurons
results <- (pr.nn_)*(max(allData$KFC_Temperature)-min(allData$KFC_Temperature))+min(allData$KFC_Temperature)##What the net predicts test should be

##Load Missing Data
qmaxs <- apply(question, 2, max) 
qmins <- apply(question, 2, min)
qscaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))
questionComputed <- compute(nn,qscaled[,1:10])
questionComputed_ <- questionComputed$net.result*(max(train_$KFC_Temperature)-min(train_$KFC_Temperature))+min(train_$KFC_Temperature)##values in neurons
results <- (questionComputed_)*(max(allData$KFC_Temperature)-min(allData$KFC_Temperature))+min(allData$KFC_Temperature)##What the net predicts test should be
View(results)
