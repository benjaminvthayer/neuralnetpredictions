library(neuralnet)
## Find KFC Radiation ##
allData<-Find_ARBRC_TemperatureAll
testTrain <-Find_ARBRC_TemperatureTestTrain
question <- Find_ARBRC_TemperatureMissingData
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)
question<-question[rowSums(is.na(question)) != ncol(question),]
#allData<-allData[rowSums(is.na(allData)) != ncol(allData),]
#testTrain<-testTrain[rowSums(is.na(testTrain)) != ncol(testTrain),]

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("ARBRC_Temperature_F ~", paste(n[!n %in% "ARBRC_Temperature_F"], collapse = " + ")))
nn <- neuralnet(f,data=trainScaled,hidden=c(2),linear.output=T,stepmax=1e7)

##Transform back to Usable Values
pr.nn <- compute(nn,testScaled[,1:4])
testResults<-(pr.nn$net.result)*(max(allData$ARBRC_Temperature_F)-min(allData$ARBRC_Temperature_F))+min(allData$ARBRC_Temperature_F)##What the net predicts test should be

##Load Missing Data
questionComputed <- compute(nn,questionScaled[,1:4])
results <- (questionComputed$net.result)*(max(allData$ARBRC_Temperature_F)-min(allData$ARBRC_Temperature_F))+min(allData$ARBRC_Temperature_F)##What the net predicts test should be


plot(nn,arrow.length = 0.2,show.weights = TRUE)
saveRDS(file="weights.txt",nn)
write.table(test, "clipboard-16384", sep="\t", row.names=FALSE)
write.table(testResults, "clipboard-16384", sep="\t", row.names=FALSE)
write.table(results, "clipboard-16384", sep="\t", row.names=FALSE)
