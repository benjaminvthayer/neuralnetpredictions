library(neuralnet)
## Find KFC Radiation ##
allData<-Find_KFC_PrecipitationAll
testTrain <-Find_KFC_PrecipitationTestTrain
question <- Find_KFC_PrecipitationMissingData
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("KFC_Precipitation ~", paste(n[!n %in% "KFC_Precipitation"], collapse = " + ")))
nn <- neuralnet(f,data=trainScaled,hidden=c(8),linear.output=T,stepmax=1e7)

##Transform back to Usable Values
pr.nn <- compute(nn,testScaled[,1:11])
testResults<- (pr.nn$net.result)*(max(allData$KFC_Precipitation)-min(allData$KFC_Precipitation))+min(allData$KFC_Precipitation)##What the net predicts test should be

##Load Missing Data
questionComputed <- compute(nn,questionScaled[,1:11])
results <- (questionComputed$net.result)*(max(allData$KFC_Precipitation)-min(allData$KFC_Precipitation))+min(allData$KFC_Precipitation)##What the net predicts test should be


View(results)
plot(nn,arrow.length = 0.2,show.weights = FALSE)
write.table(test, "clipboard", sep="\t", row.names=FALSE)
write.table(testResults, "clipboard", sep="\t", row.names=FALSE)
write.table(results, "clipboard", sep="\t", row.names=FALSE)
