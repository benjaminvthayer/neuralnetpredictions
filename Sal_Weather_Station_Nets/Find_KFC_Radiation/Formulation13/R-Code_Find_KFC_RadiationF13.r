library(neuralnet)
## Find KFC Radiation ##
allData<-Find_KFC_RadiationAll
testTrain <-Find_KFC_RadiationTestTrain
question <- Find_KFC_RadiationMissingData
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("KFC_Radiation_Wm2 ~", paste(n[!n %in% "KFC_Radiation_Wm2"], collapse = " + ")))
nn <- neuralnet(f,data=trainScaled,hidden=c(5),linear.output=T,stepmax=1e8,threshold=.03)

##Transform back to Usable Values
pr.nn <- compute(nn,testScaled[,1:10])
testResults<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be

##Load Missing Data
questionComputed <- compute(nn,questionScaled[,1:10])
results <- (questionComputed$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be

nn$weights
setwd("C:\\Users\\Ben\\Documents\\gitRepos\\ssol\\neuralnetpredictions\\Sal_Weather_Station_Nets\\Find_KFC_Radiation\\Formulation13")
saveRDS(file="r_readable_net.txt",nn)
nn<-read(file="r_readable_net.rda",nn)
plot(nn,arrow.length = 0.2,show.weights = FALSE)
write.table(test, "clipboard", sep="\t", row.names=FALSE)
write.table(testResults, "clipboard", sep="\t", row.names=FALSE)
write.table(results, "clipboard", sep="\t", row.names=FALSE)
