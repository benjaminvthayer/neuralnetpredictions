

library(neuralnet)
library("Metrics")
## Find KFC Radiation ##
allData<-read.csv('Find_KFC_Radiation_All.csv')
testTrain<-read.csv('Find_KFC_RadiationTestTrain.csv')
question<-read.csv('Find_KFC_RadiationMissingData.csv')
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

thresh<-".02"
nodes<-"7"
algo<-"backprop"

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(testTrain))
set.seed(123)
train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
train <- testTrain[train_ind, ]
test <- testTrain[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("KFC_Radiation_Wm2 ~", paste(n[!n %in% "KFC_Radiation_Wm2"], collapse = " + ")))


#Create container to store stats of best ANN
resultsPre <- rep(0, 7)
results <- array(resultsPre, c(2,7))

results[1,1]<-"Train RMSE"
results[1,2]<-"Train R^2"
results[1,3]<-"Test RMSE"
results[1,4]<-"Test R^2"
results[1,5]<-"Algorithm"
results[1,6]<-"Hidden Nodes"
results[1,7]<-"Threshold"

results[2,5]<-algo
results[2,6]<-nodes
results[2,7]<-thresh

##create 100 ANNs with same scenario, remember ANN with best Weights
bestaccuracy<-9999
for(x in 1:100){
  nn <- neuralnet(f,data=trainScaled,hidden=nodes,linear.output=T,stepmax=1e8,threshold=thresh,learningrate=.001,algorithm=algo)
  pr.nn <- compute(nn,testScaled[,1:10])
  testPredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be
  testActual<-test$KFC_Radiation_Wm2
  
  ##compute test stats
  pr.nn <- compute(nn,testScaleds[,1:10])
  testPredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be                                      
  testActual<-testLists[,outputVarNum]
  currentRMSTest<-rmse(testActual,testPredicted)
  testRsqr<-1 - (sum((testActual-testPredicted )^2)/sum((testActual-mean(testActual))^2))
  
  #compute train stats
  pr.nn <- compute(nn,trainScaleds[,1:10])
  trainPredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be
  trainActual<-trainLists[,outputVarNum]
  RMSTrain<-rmse(trainActual,trainPredicted)
  trainRsqr<-1 - (sum((trainActual-trainPredicted )^2)/sum((trainActual-mean(trainActual))^2))##r^2
  
  currentAccuracyScore<-((1/testRsqr)+currentRMSTest + (1/trainRsqr)+RMSTrain )/4#lower is better
  
  if(currentAccuracyScore<bestaccuracy){
    bestnn<-nn
    results[2,1]<-RMSTrain
    results[2,2]<-trainRsqr
    results[2,3]<-currentRMSTest
    results[2,4]<-testRsqr
  }
}


#Compute Missing Data
questionComputed <- compute(bestnn,questionScaled[,1:10])
MissingData <- (questionComputed$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be


#Save Report of ANN
write.csv(results,file="KFC_RadF16_Results.csv")
#Save weights
capture.output(bestnn$weights, file = "KFC_RadF16_Weights.txt")
#save the ANN in a format to be re-loaded by r
saveRDS(file="KFC_RadF16_r_readable.txt",bestnn)
#Save Prediction For Missing Data
write.csv(MissingData,file="KFC_RadF16_FinalMissingPrediction.csv")



