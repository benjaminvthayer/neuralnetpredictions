#install.packages(c("neuralnet"),lib=("/uufs/chpc.utah.edu/common/home/$USER/software/pkg/RLibs/3.4.4i"),repos=c("http://cran.us.r-project.org"),verbose=TRUE)
#install.packages(c("Metrics"),lib=("/uufs/chpc.utah.edu/common/home/$USER/software/pkg/RLibs/3.4.4i"),repos=c("http://cran.us.r-project.org"),verbose=TRUE)


library(neuralnet)
library("Metrics")
setwd("C:\\Users\\Ben\\Documents\\gitRepos\\ssol\\neuralnetpredictions\\Sal_Weather_Station_Nets\\Find_KFC_Radiation\\Formulation16")
## Find KFC Radiation ##
allData<-read.csv('Find_KFC_Radiation_All.csv')
testTrain<-read.csv('Find_KFC_RadiationTestTrain.csv')
question<-read.csv('Find_KFC_RadiationMissingData.csv')
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

##create containers for all test and train values
numPartitions<-20
variables<-11
statNum<-8##number of stats
trainSize<-468
testSize<-118
testListsPre <- rep(0, testSize*variables*numPartitions)  
testLists <- array(testListsPre, c(testSize,variables,numPartitions)) 
trainListsPre <- rep(0, trainSize*variables*numPartitions)
trainLists <- array(trainListsPre, c(trainSize,variables,numPartitions))

##compute all train and tests
#trainlists is [468,11,20]  ([days,weatherRecordingTypes,partitions])
#testlists is [118,11,20]  ([days,weatherRecordingTypes,partitions])
#testTrain is [586,11]   ([days,weatherRecordingTypes])
for(partitionSeed in 1:numPartitions){

  trainLists[,,partitionSeed] <-testTrain[train_ind, ]
  testLists [,,partitionSeed] <- testTrain[-train_ind, ]
}#to delete

for(partitionNum in 1:numPartitions){
  smp_size <- floor(0.80 * nrow(testTrain))
  set.seed(partitionSeed)
  train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)   
  for(dataColumn in 1:variables){
    for(day in 1:testSize){
      testLists[day][dataColumn][partitionNum]<-testTrain[day, dataColumn]
    }
    for(day in 1:trainSize){
      trainLists[day][dataColumn][partitionNum]<-testTrain[day, dataColumn]
    }
  }
}


testScaledsPre <- rep(0, testSize*variables*numPartitions)
testScaleds <-  array(testScaledsPre, c(testSize,variables,numPartitions)) 
trainScaledsPre <-rep(0, trainSize*variables*numPartitions)
trainScaleds <- array(trainScaledsPre, c(trainSize,variables,numPartitions))

##Normalize The Data
for(partitionSeedNum in 1:numPartitions){
  trainScaleds[][][partitionSeedNum] <- as.data.frame(scale(trainLists[][][partitionSeedNum], center = mins, scale = maxs - mins))
  testScaleds[,,partitionSeedNum]  <- as.data.frame(scale(testLists[,,partitionSeedNum], center = mins, scale = maxs - mins))
  questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))
}#to delete

maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaleds<-scale(trainLists, center = mins, scale = maxs - mins)
for(dataColumn in 1:variables){
  for(partitionNum in 1:numPartitions){
    for(day in 1:trainSize){
      trainScaleds[day][dataColumn][partitionNum]<-(trainLists[day][dataColumn][partitionNum]-mins[[dataColumn]])/(max[[dataColumn]]-min[[dataColumn]])
    }
  }
}

##Create the formula
n <- names(trainScaled)
f <- as.formula(paste("KFC_Radiation_Wm2 ~", paste(n[!n %in% "KFC_Radiation_Wm2"], collapse = " + ")))

##container for results
##statistic numbers: 1 partitionSeed,2 algorithm,3 hiddenNodes,4 threshol,5 rmse training, 6 r^2 train,7 rmse test, 8 r^2 test
scenarioStatsPre <- rep(0, numPartitions)  
scenarioStats <- array(scenarioStatsPre, c(statNum,numPartitions)) 

bestAccuracyScore<-999999#average of test rootmeansquared, train rms, 1/test r^2, and 1/train r^2

##determine the scenario parameters from the command line-provided scenario number
scenarioNum<-commandArgs()
algoNum<-scenarioNum%%20+1
threshNum<-(scenarioNum%/%5)%%10+1
nodeNum<-(scenarioNum%/%10)%%10+1
algos<-c('rprop-','rprop+','sag','backprop','default')
threshs<-c(.02,.03)
nodeNumbers<-c(2:11)
algo<-algos[algoNum]
thresh<-threshs[threshNum]
node<-nodeNumbers[nodeNum]

for(partitionSeedPos in 1:numPartitions){
  if(algo=='backprop'){
    nn <- neuralnet(f,data=trainScaled,hidden=c(node),linear.output=T,stepmax=1e8,threshold=thresh,learningrate=.001,algorithm=algo)
  }
  else if(algo=='default'){
    nn <- neuralnet(f,data=trainScaled,hidden=c(node),linear.output=T,stepmax=1e8,threshold=thresh)
  }
  else{##rprop + or - or sag
    nn <- neuralnet(f,data=trainScaled,hidden=c(node),linear.output=T,stepmax=1e8,threshold=thresh,algorithm=algo)
  }
  
  ##compute test stats
  pr.nn <- compute(nn,testScaled[,1:10])
  testPredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be
  testActual<-test$KFC_Radiation_Wm2
  currentRMSTest<-rmse(testActual,testPredicted)
  testRsqr<-1 - (sum((testActual-testPredicted )^2)/sum((testActual-mean(testActual))^2))
  
  #compute train stats
  pr.nn <- compute(nn,trainScaled[,1:10])
  trainPredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be
  trainActual<-train$KFC_Radiation_Wm2
  RMSTrain<-rmse(trainActual,trainPredicted)
  trainRsqr<-1 - (sum((trainActual-trainPredicted )^2)/sum((trainActual-mean(trainActual))^2))##r^2
  
  currentAccuracyScore<-((1/testRsqr)+currentRMSTest + (1/trainRsqr)+RMSTrain )/4
  
  ##store if best
  if(currentAccuracyScore<bestAccuracyScore){
    bestnn<-nn
    bestRMSTest<-currentRMSTest
    bestAccuracyScore<-currentAccuracyScore
    bestAlgo<-algo
    bestThresh<-thresh
    bestNode<-nodes
  }
  ##store
  ##statistic numbers: 1 partitionSeed,2 algorithm,3 hiddenNodes,4 threshold,5 rmse training, 6 r^2 train,7 rmse test, 8 r^2 test
  scenarioStats[1,partitionSeedPos]<- partitionSeedPos
  scenarioStats[2,partitionSeedPos]<- algo
  scenarioStats[3,partitionSeedPos]<- nodes
  scenarioStats[4,partitionSeedPos]<- thresh
  scenarioStats[5,partitionSeedPos]<- RMSTrain
  scenarioStats[6,partitionSeedPos]<- trainRsqr
  scenarioStats[7,partitionSeedPos]<- currentRMSTest
  scenarioStats[8,partitionSeedPos]<- testRsqr
}

(outFileName <- paste('Find_KFC_RadiationF16#',toString(scenarioNum),'.csv'))
write.csv(scenarioStats,file=outFileName)

