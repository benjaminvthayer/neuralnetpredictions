library(neuralnet)
library("Metrics")
## Find KFC Radiation ##
allData<-Find_KFC_Radiation_All
testTrain <-Find_KFC_RadiationTestTrain
question <- Find_KFC_RadiationMissingData
allData <- Filter(function(x)!all(is.na(x)), allData)
testTrain <- Filter(function(x)!all(is.na(x)), testTrain)
question <- Filter(function(x)!all(is.na(x)), question)

##Divide into Test and Train

##create containers for all test and train values
numPartitions<-20
statNum<-8##number of stats
trainSize<-468
testSize<-118
testListsPre <- rep(0, testSize*numPartitions)  
testLists <- array(testListsPre, c(testSize,numPartitions)) 
trainListsPre <- rep(0, trainSize*numPartitions)
trainLists <- array(trainListsPre, c(trainSize,numPartitions))

##compute all train and tests
for(partitionSeed in 1:numPartitions){
  smp_size <- floor(0.80 * nrow(testTrain))
  set.seed(partitionSeed)
  train_ind <- sample(seq_len(nrow(testTrain)), size = smp_size)
  trainLists[,partitionSeed] <- testTrain[train_ind, ]$KFC_Radiation_Wm2
  testLists [,partitionSeed] <- testTrain[-train_ind, ]$KFC_Radiation_Wm2
}

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("KFC_Radiation_Wm2 ~", paste(n[!n %in% "KFC_Radiation_Wm2"], collapse = " + ")))

##create container for scenario statistics
##[scenarioNum,statNum,partiotionNum]
##statistic numbers: 1 partitionSeed,2 algorithm,3 hiddenNodes,4 threshol,5 rmse training, 6 r^2 train,7 rmse test, 8 r^2 test
numScenarios<-100+1##one greater than actual number of scenarios
scenarioStatsPre <- rep(0, numScenarios*statNum*numPartitions)  
scenarioStats <- array(scenarioStatsPre, c(numScenarios,statNum,numPartitions)) 

bestRMSTest<-999999##root mean squared error
scenarioNum<-1
##compute with all parameters in several ranges
for(algo in c('rprop-','rprop+','sag','backprop','default')){ ##
  for(thresh in c(.02,.03)) {0
    for (nodes in c(2:11)) {
      
      for(partitionSeedPos in 1:numPartitions){
        if(algo=='backprop'){
          nn <- neuralnet(f,data=trainScaled,hidden=c(nodes),linear.output=T,stepmax=1e8,threshold=thresh,learningrate=.001,algorithm=algo)
        }
        else if(algo=='default'){
          nn <- neuralnet(f,data=trainScaled,hidden=c(nodes),linear.output=T,stepmax=1e8,threshold=thresh)
        }
        else{##rprop + or - or sag
          nn <- neuralnet(f,data=trainScaled,hidden=c(nodes),linear.output=T,stepmax=1e8,threshold=thresh,algorithm=algo)
        }
        
        ##compute test stats
        pr.nn <- compute(nn,testScaled[,1:10])
        testPredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be
        testActual<-test$KFC_Radiation_Wm2
        currentRMSTest<-rmse(testActual,testPredicted)
        testRsqr<-1 - (sum((testActual-testPredicted )^2)/sum((testActual-mean(testActual))^2))
        
        #compute train stats
        pr.nn <- compute(nn,trainScaled[,1:10])
        trainPredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be
        trainActual<-train$KFC_Radiation_Wm2
        RMSTrain<-rmse(trainActual,trainPredicted)
        trainRsqr<-1 - (sum((trainActual-trainPredicted )^2)/sum((trainActual-mean(trainActual))^2))##r^2
        
        if(currentRMSTest<bestRMSTest){
          bestnn<-nn
          bestRMSTest<-currentRMSTest
          bestAlgo<-algo
          bestThresh<-thresh
          bestNode<-nodes
        }
        ##store
        ##statistic numbers: 1 partitionSeed,2 algorithm,3 hiddenNodes,4 threshold,5 rmse training, 6 r^2 train,7 rmse test, 8 r^2 test
        
        scenarioStats[scenarioNum,1,partitionSeedPos]<- partitionSeedPos
        scenarioStats[scenarioNum,2,partitionSeedPos]<- algo
        scenarioStats[scenarioNum,3,partitionSeedPos]<- nodes
        scenarioStats[scenarioNum,4,partitionSeedPos]<- thresh
        scenarioStats[scenarioNum,5,partitionSeedPos]<- RMSTrain
        scenarioStats[scenarioNum,6,partitionSeedPos]<- trainRsqr
        scenarioStats[scenarioNum,7,partitionSeedPos]<- currentRMSTest
        scenarioStats[scenarioNum,8,partitionSeedPos]<- testRsqr
      }
      scenarioNum<-scenarioNum+1
    }
  }
}

 ##container for final total statistics
totalStatsPre<-rep(0, (numScenarios)*4);                                     
totalStats <- array(totalStatsPre, c((numScenarios),4)); 


##average totals
for(scenario in 1:(numScenarios)){
  totalStats[scenario,1]<- mean(as.numeric(scenarioStats[scenario,5,]))  ##rmstrain
  totalStats[scenario,2]<- mean(as.numeric(scenarioStats[scenario,6,]))  ##train r^2
  totalStats[scenario,3]<- mean(as.numeric(scenarioStats[scenario,7,]))  ##rmstest
  totalStats[scenario,4]<- mean(as.numeric(scenarioStats[scenario,8,]))  ##test r^2
}



##predict test data using best run
pr.nn <- compute(bestnn,testScaled[,1:10])
bestpredicted<- (pr.nn$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be

##Load Missing Data
questionComputed <- compute(bestnn,questionScaled[,1:10])
results <- (questionComputed$net.result)*(max(allData$KFC_Radiation_Wm2)-min(allData$KFC_Radiation_Wm2))+min(allData$KFC_Radiation_Wm2)##What the net predicts test should be

bestnn$weights
setwd("C:\\Users\\Ben\\Documents\\gitRepos\\ssol\\neuralnetpredictions\\Sal_Weather_Station_Nets\\Find_KFC_Radiation\\Formulation13d")
saveRDS(file="r_readable_net.txt",bestnn)
nn<-readRDS(file="r_readable_net.txt",nn)
plot(bestnn,arrow.length = 0.2,show.weights = FALSE)
write.table(test, "clipboard", sep="\t", row.names=FALSE)
write.table(scenarioStats, "clipboard-16384", sep="\t", row.names=FALSE)
write.table(totalStats, "clipboard", sep="\t", row.names=FALSE)
write.table(bestpredicted, "clipboard", sep="\t", row.names=FALSE)
write.table(results, "clipboard", sep="\t", row.names=FALSE)
