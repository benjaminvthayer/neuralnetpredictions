library(neuralnet)
## Load Data##
allData<-AllData
##question <- questionData
allData <- Filter(function(x)!all(is.na(x)), allData)
##question <- Filter(function(x)!all(is.na(x)), question)

##Divide into Test and Train
smp_size <- floor(0.80 * nrow(allData))
set.seed(123)
train_ind <- sample(seq_len(nrow(allData)), size = smp_size)
train <- allData[train_ind, ]
test <- allData[-train_ind, ]

##Normalize The Data
maxs <- apply(allData, 2, max) 
mins <- apply(allData, 2, min) 
trainScaled <- as.data.frame(scale(train, center = mins, scale = maxs - mins))
testScaled  <- as.data.frame(scale(test, center = mins, scale = maxs - mins))
##questionScaled <- as.data.frame(scale(question, center = mins, scale = maxs - mins))

##Create the Net
n <- names(trainScaled)
f <- as.formula(paste("Pervious_Percent ~", paste(n[!n %in% "Pervious_Percent"], collapse = " + ")))
nn <- neuralnet(f,data=trainScaled,hidden=c(7),linear.output=T,stepmax=1e9)

##,learningrate=.05,likelihood = TRUE,linear.output=FALSE

##Transform back to Usable Values
pr.nn <- compute(nn,testScaled[,1:8])
testResults<- (pr.nn$net.result)*(max(allData$Pervious_Percent)-min(allData$Pervious_Percent))+min(allData$Pervious_Percent)##What the net predicts test should be

##Load Missing Data
##questionComputed <- compute(nn,questionScaled[1:8])
##results <- (questionComputed$net.result)*(max(allData$Pervious_Percent)-min(allData$Pervious_Percent))+min(allData$Pervious_Percent)##What the net predicts test should be

nn$weights
plot(nn,arrow.length = 0.2,show.weights = TRUE)
write.table(test, "clipboard", sep="\t", row.names=FALSE)
write.table(testResults, "clipboard", sep="\t", row.names=FALSE)
##write.table(results, "clipboard", sep="\t", row.names=FALSE)
